#!/usr/bin/python3
"""
Convert a mail to plaintext - this is nice if a mail should copied to a ticket 
system including header information like TO, CC, FROM, SUBJECT, DATE.

Step 1: copy the source code of the mail to the clipboard.
Step 2: start stripEmail.py
Step 3: paste the clipboard somewhere (e.g. in the ticket system)

HTML mails and a lot of various different encodings are converted to plain text.
"""

# imports datetime to format the date of the mail
import datetime
# imports pyperclip to read and write to the clipboard
import pyperclip
# imports mailparser to parse the raw mail
import mailparser

# stores the raw mail read from the clipboard
mailsource = pyperclip.paste()

# stores te parsed mail object from the previously read raw mail
mail = mailparser.parse_from_string(mailsource)

# stores the plaintext mail without html or formatting
plainmail = ""

# opens the pre tag
plainmail += "<pre>"
# adds a newline to the plainmail
plainmail += "\n"
# adds the senders to the plainmail
plainmail += "FROM: " + (", ".join(sender[1] for sender in mail.from_))
# adds a newline to the plainmail
plainmail += "\n"
# adds the receivers to the plainmail
plainmail += "TO: " + (", ".join(sender[1] for sender in mail.to))
# adds a newline to the plainmail
plainmail += "\n"
# checks if the mail has cc receivers
if mail.cc:
    # adds the cc receivers to the plainmail
    plainmail += "CC: " + (", ".join(sender[1] for sender in mail.cc))
    # adds a newline to the plainmail
    plainmail += "\n"
# adds the subject to the plainmail
plainmail += "SUBJECT: " + mail.subject
# adds a newline to the plainmail
plainmail += "\n"
# reads the date
try:
    # adds the date to the plainmail if existant and readable
    plainmail += "DATE: " + mail.date.strftime("%d.%m.%Y - %H:%M:%S")
    # adds a newline to the plainmail
    plainmail += "\n"
except:
    # passes if reading the date failes
    pass
# adds a newline to the plainmail
plainmail += "\n"
# adds the body to the plain mail and removes html
plainmail += " ".join(mail.text_plain)
# adds a newline to the plainmail
plainmail += "\n"
# closes the pre tag
plainmail += "</pre>"

# copies the plainmail to the clipboard
pyperclip.copy(plainmail)